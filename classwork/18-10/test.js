let name = "Roman";

/*console.log(name.toUpperCase());
console.log(name.indexOf("m"));
console.log(name.lastIndexOf("n")); // метод который считает с конца, но числовые значения - индексы те же
console.log(name.includes("o", 3)); // второй аргумент в методе Includes указывает индекс с которого нужно считать*/ 

/*console.log(name.startsWith("Ro")); //Начинается ли строка с этих символов
console.log(name.endsWith("n")); // заканчивается ли строка этими символами*/

/*let newName = name.slice(0,4);// первое число от которого считаем, второе число до которого считаем, но его не включаем
console.log(newName);

let newName = name.substring(0,4);// первое число от которого считаем, второе число до которого считаем, но его не включаем
console.log(newName);

let newName = name.substr(0,4);// первое число от которого считаем, второе число, которое означает количество символов
console.log(newName);*/

/*let newName2 = name.slice(0,2).repeat(3); // repeat - метод, который повторяет строку количество раз, указанное в аргументе
console.log(newName2);

console.log("A".charCodeAt(0).toString()); //charCodeAt() возврящает код символа
console.log("a".charCodeAt(0).toString());*/

console.log(name.replace("Ro", "Or"));