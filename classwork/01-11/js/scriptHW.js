"use strict"

const arr = [1, 2, 3, 4];

const arrItems = arr.map((el) => {
    let item = document.createElement("li");
    item.textContent = el;
    return item;
});

console.log(arrItems);

const fragment = document.createDocumentFragment();
fragment.append(...arrItems);

// ... -> это продолжение следует

console.log(fragment);