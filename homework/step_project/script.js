"use strict";

const tabs = document.querySelectorAll(".tab-title");
const tabsContent = document.querySelectorAll(".tab-content");

tabs.forEach((tab, i) =>
  tab.addEventListener("click", () => {
    if (!tab.classList.contains("active")) {
      tabs.forEach((elem) => elem.classList.remove("active"));
      tab.classList.add("active");
      tabsContent.forEach((elem) =>
        elem.classList.remove("active-tab-content")
      );
      tabsContent[i].classList.add("active-tab-content");
    }
  })
);
