"use strict";

const eyeIcons = document.querySelectorAll(".fa-eye");
const inputs = document.querySelectorAll(".password-form-input");
const btn = document.querySelector(".btn");

let newDiv = document.createElement("div");
newDiv.innerHTML = "<p>Нужно ввести одинаковые значения</p>";
newDiv.style.color = "red";
newDiv.style.display = "none";
newDiv.classList.add("error");
btn.before(newDiv);

eyeIcons.forEach((elem, i) => {
  elem.addEventListener("click", (e) => {
    elem.classList.toggle("fa-eye-slash");
    if (inputs[i].getAttribute("type") === "password") {
      inputs[i].setAttribute("type", "text");
    } else if (inputs[i].getAttribute("type") === "text") {
      inputs[i].setAttribute("type", "password");
    }
  });
});

btn.addEventListener("click", (e) => {
  e.preventDefault();
  if (inputs[0].value === inputs[1].value) {
    newDiv.style.display = "none";
    alert("You are welcome");
  } else {
    newDiv.style.display = "block";
  }
});
