"use strict";

const eyeIcons = document.querySelectorAll(".fa-eye");
const inputs = document.querySelectorAll(".password-form-input");
const btn = document.querySelector(".btn");

eyeIcons.forEach((elem, i) => {
  elem.addEventListener("click", () => {
    elem.classList.toggle("fa-eye-slash");
    if (inputs[i].getAttribute("type") === "password") {
      inputs[i].setAttribute("type", "text");
    } else if (inputs[i].getAttribute("type") === "text") {
      inputs[i].setAttribute("type", "password");
    }
  });
});

btn.addEventListener("click", (e) => {
  e.preventDefault();
  if (inputs[0].value === inputs[1].value) {
    alert("You are welcome");
    newDiv.remove();
  } else {
    let newDiv = document.createElement("div");
    newDiv.innerHTML = "<h2>Нужно ввести одинаковое значение</h2>";
    newDiv.style.color = "red";
    btn.before(newDiv);
    setTimeout(() => newDiv.remove(), 2000);
  }
});
