"use strict"

//по 2 варинта для тестирования
const arrFirst = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const arrSecond = ["1", "2", "3", "sea", "user", 23];
const arrThird = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
const parent = "p";
const parent2 = "li";
const parent3 = "h1";

function addLiElements(parent, array) {

    //переменная, которая находит нужный элемент в DOM
    let parentTagName = document.getElementsByTagName(`${parent}`)[0];
    //проверка на пустую строку
    // if (parent == "") {
    //     parentTagName = document.body;
    // }

    //переменная - массив, который создали с помощью метода массива .map(true), который переберает массив и возвращает новый, который соответввует условию

    let arrNew = array.map((char) => {
            //переменная, которая создает елемент в DOM
            let elem = document.createElement("li");
            //записываем в эту переменную текст из перебранного елемента массива 
            elem.textContent = char;
            //добаваляем после элемента, который мы добавили в переменную, новую переменную с тестом из элемента массива
            parentTagName.after(elem);


            // let subArray = array.map(Array.isArray(char) => console.log(char));
            // if (Array.isArray(char)) {
            //     addLiElements(elem, char)
            //     console.log(`this is array ${char}`);
            // }

            return elem;
        }
        // if (Array.isArray(char)) {
        //     // addLiElements(elem, char)
        //     console.log(char);
        // }
    );
}
//выводи в консоль результаты вызова функции
console.log(addLiElements(parent2, arrThird));

let subArray = arrThird.map( curentValue => {
    if (Array.isArray(curentValue) == true) {
        addLiElements(parent, arrThird);
    }
    // return curentValue;
});
console.log(subArray);