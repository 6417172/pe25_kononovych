"use strict"

let block = document.getElementById("block");
let price = document.getElementById("price");
let span;

price.addEventListener("focus", (event) => {
  event.target.style.borderColor = "green";
  let elem = document.getElementById("new");
  if(elem != null) {
  elem.remove();
  event.target.value = "";
    }
  }
);

price.addEventListener("blur", handler);

function handler (event) {
  event.target.style.borderColor = "";
  span = document.createElement("span");
  span.setAttribute("id", "new")
  span.textContent = `Текущая цена: ${price.value}`;
  block.append(span);
  let btn = document.createElement("input");
  btn.id = "btn";
  btn.type = "button";
  btn.value = "X";
  span.append(btn);
  price.style.backgroundColor = "green";
  btn.addEventListener("click", (event) => {
    price.value = "";
    span.remove();
    price.style.backgroundColor = "";
  });
  if(price.valueAsNumber <= 0) {
    price.style.borderColor = "red";
    let textNode = document.createTextNode("Please enter correct price");
    let textBlock = document.createElement("div");
    textBlock.id = "textBlock";
    price.after(textBlock);
    textBlock.append(textNode);
    span.remove();
    price.style.backgroundColor = "";
  } else {
    let textBlock = document.getElementById("textBlock");
    if(textBlock !== null) {
    textBlock.remove();
    }
  }
}
