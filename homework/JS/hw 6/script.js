let arr =['21',true, 15, 'ghbdtn'];
let typeData = 'number';
// const newArr = [];

function filterBy(arr, typeData) {
	const newArr = [];
	for( let i=0; i <arr.length; i++) {
		let type = typeof arr[i];
		if (type !== typeData) {
			newArr.push(arr[i]);
		}
	}
	return newArr;
}

console.log(filterBy(arr, typeData));

//если массив объявлен в функции, после вызова функции, не могу вызвать массив newArr, 
//получатся только, если массив - глобальная переменная 