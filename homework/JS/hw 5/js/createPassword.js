"use strict";

const firstName = prompt("name");
const lastName = prompt("surname");
const birthday = prompt("birthday");
const password = getPassword(firstName, lastName, birthday);

function getPassword(firstName, lastName, birthday) {
	const name = firstName.slice(0, 1).toUpperCase();
	const surname = lastName.toLowerCase();
	const year = birthday.slice(6);

	const password = name + surname + year;
	return password;
}

console.log(password);