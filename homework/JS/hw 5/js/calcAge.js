"use strict";


// console.log (now);

// console.log(Date.now());

// console.log(now.getDate());

// console.log(now.getHours());

// console.log(now.getMonth());

const date1 = new Date ("2020-10-22");
// console.log(date1);

const date2 = new Date (Date.now());
// console.log(date2);

// console.log((date2 - date1) / (1000*60*60));

// console.log(date2.getFullYear());

const birthday = prompt("Put your birthday", "dd.mm.yyyy");

function getAge(birthday) {
	const now = new Date();
	const dayBirthday = birthday.slice(0,2);
	const monthBirthday = birthday.slice(3,5);
	const yearBirthday = birthday.slice(6);

	const strCorrectDate = yearBirthday+"-"+monthBirthday+"-"+dayBirthday;
	// console.log(strCorrectDate);
	const correctDate = new Date(strCorrectDate);
	// console.log(correctDate);
	const age = (Math.floor((now - correctDate)/ (1000*60*60*24*365)));
	return age;
};

console.log(getAge(birthday));