"use strict";
const btns = document.querySelectorAll(".btn");

document.addEventListener("keydown", (e) => {
  btns.forEach((btn) => {
    if (btn.textContent === e.code || btn.textContent.toLowerCase() === e.key) {
      btns.forEach((elem) => {
        elem.classList.remove("active");
      });
      btn.classList.add("active");
    }
  });
});
